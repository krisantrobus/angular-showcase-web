# Angular Showcase

The purpose of this repository is to demonstrate Angular knowledge building applicaitons from open data.

## Firebase

Firebase integration for hosting has been acheived. Run the following to build and deploy, visit: [https://showcase-angular.firebaseapp.com](https://showcase-angular.firebaseapp.com).

```
ng build --prod && firebase deploy
```

## Routing

Each module should have an indivudual lazy loaded route. Each sub app should have it's own route and module using dropdowns for navbar. Only key features should appear on the app bar for mobile.

## State & NgRx

State management for the entrire application is acheived using NgRx. There should be a root reducer and an initial null state. Each sub app should have it's own reducers and selectors to manage the segment of data it requires.
