export enum COIN_TYPES {
  crypto = "CRYPTO",
  fiat = "FIAT",
}

export enum COIN_STATUS {
  active = "ACTIVE",
  inactive = "INACTIVE",
}

export interface AssetSchema {
  id: string;
  name: string;
  status: COIN_STATUS;
  symbol: string;
  type: COIN_TYPES;
  url: string;
}

export interface AssetTickerSchema {
  assetId: string;
  freeFloatSupply: string;
  id: string;
  marketCapPercentChange: {
    change24h: number;
    change30d: number;
    change7d: number;
  };
  marketCap: string;
  marketCapRank: number;
  price: string;
  pricePercentChange: {
    change24h: number;
    change30d: number;
    change7d: number;
  };
  timestamp: Date;
  totalMarketCap: string;
  totalMarketCapPercentChange: {
    change24h: number;
    change30d: number;
    change7d: number;
  };
  totalSupply: string;
  volume: string;
  volumePercentChange: {
    change24h: number;
    change30d: number;
    change7d: number;
  };
  volumeRank: number;
}

export class Coin {
  symbol: string;
  name: string;
  type: string;
  url: string;
  status: string;

  assetId: string;
  freeFloatSupply: string;
  id: string;
  marketCapPercentChange: {
    change24h: number;
    change30d: number;
    change7d: number;
  };
  marketCap: string;
  marketCapRank: number;
  price: string;
  pricePercentChange: {
    change24h: number;
    change30d: number;
    change7d: number;
  };
  timestamp: Date;
  totalMarketCap: string;
  totalMarketCapPercentChange: {
    change24h: number;
    change30d: number;
    change7d: number;
  };
  totalSupply: string;
  volume: string;
  volumePercentChange: {
    change24h: number;
    change30d: number;
    change7d: number;
  };
  volumeRank: number;

  constructor() {}

  public setTickerDetails(res: AssetTickerSchema): Coin {
    this.freeFloatSupply = res.freeFloatSupply;
    this.id = res.id;
    this.marketCapPercentChange = res.marketCapPercentChange;
    this.marketCap = res.marketCap;
    this.marketCapRank = res.marketCapRank;
    this.price = res.price;
    this.pricePercentChange = res.pricePercentChange;
    this.timestamp = res.timestamp;
    this.totalMarketCap = res.totalMarketCap;
    this.totalMarketCapPercentChange = res.totalMarketCapPercentChange;
    this.totalSupply = res.totalSupply;
    this.volume = res.volume;
    this.volumePercentChange = res.volumePercentChange;
    this.volumeRank = res.volumeRank;

    return this;
  }

  public setAssetDetails(res: AssetSchema): Coin {
    this.assetId = res.id;
    this.name = res.name;
    this.status = res.status;
    this.symbol = res.symbol;
    this.type = res.type;
    this.url = res.url;

    return this;
  }
}
