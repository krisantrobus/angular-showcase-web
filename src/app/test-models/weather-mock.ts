import { Weather } from "../modules/weather-module/model";

export const MOCK_WEATHER: Weather = {
    clouds: { all: 75 },
    id: 1,
    main: {
        humidity: 100,
        pressure: 1029,
        temp: 11.06,
        temp_max: 12.78,
        temp_min: 8.89
    },
    name: 'Mock City',
    visibility: 6000,
    weather: [
        {
            id: 520,
            main: "Rain",
            description: "light intensity shower rain",
            icon: "09n"
        }
    ],
    wind: { speed: 2.6, deg: 340 }

}