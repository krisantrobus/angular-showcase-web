import { NgModule } from "@angular/core";
import { CardHoverDirective } from "./card-hover.directive";

// Export the component to it can be used in multiple modules
@NgModule({
  declarations: [CardHoverDirective],
  exports: [CardHoverDirective]
})
export class CardDirectiveModule { }
