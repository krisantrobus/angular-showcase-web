import { Directive, HostListener, ElementRef } from "@angular/core";

@Directive({
  selector: "[appCardHover]"
})
export class CardHoverDirective {
  constructor(private el: ElementRef) {}

  // run when dovering over the element
  @HostListener("mouseenter") onMouseEnter() {
    this.highlight(true);
  }

  // run when leaving the element
  @HostListener("mouseleave") onMouseLeave() {
    this.highlight(false);
  }

  // Highlighted based on whether the mouse is over the element by applying box shadow styling
  private highlight(active: boolean): void {
    this.el.nativeElement.style.boxShadow = active
      ? "2px 2px 12px rgba(0,0,0, 0.9)"
      : "none";

    this.el.nativeElement.style.cursor = active ? "pointer" : "none";
  }
}
