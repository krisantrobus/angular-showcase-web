import { Pipe, PipeTransform } from "@angular/core";
import { Coin } from "src/app/interfaces/types-currency";

@Pipe({
  name: "filterCurrencyList",
})
export class FilterCurrencyListPipe implements PipeTransform {
  transform(currencies: Object[], searchValue: string): any {
    if (currencies.length > 0 && searchValue) {
      let filteredList = currencies.map((currency: Coin) => {
        const currencySearchVals: string[] = [currency.name, currency.symbol];

        const IsValid =
          currencySearchVals.filter((value) => {
            if (value.toLowerCase().includes(searchValue.toLowerCase())) {
              return value;
            }
          }).length > 0;
        if (IsValid) {
          return currency;
        }
      });
      return filteredList;
    }
    return currencies;
  }
}
