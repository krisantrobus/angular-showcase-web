import { Injectable, Pipe, PipeTransform, NgModule } from "@angular/core";
import { Store, select } from "@ngrx/store";
import { Observable, Subscription } from "rxjs";
import { AppState } from "src/app/store/app.state";
import { getShowCurrency } from "src/app/modules/currency-module/store/selector";

@Injectable({
  providedIn: "root",
})
export class TranslateService {
  constructor(private _store: Store<AppState>) {}

  private region: Observable<string> = this._store.pipe(
    select(getShowCurrency)
  );
  private regionSub: Subscription;

  public getCurrencySymbolText = () => {
    let extractRegion: string;

    this.regionSub = this.region.subscribe((region) => {
      extractRegion = region;
      // this.regionSub.unsubscribe();
    });

    switch (extractRegion.toLowerCase()) {
      case "usd":
        return "$";
      default:
        return "£";
    }
  };
}

@Pipe({
  name: "formatTo2DP",
  pure: false,
})
export class TranslateFormatTo2DP implements PipeTransform {
  transform(value: string): any {
    return parseFloat(value).toFixed(2);
  }
}

@Pipe({
  name: "addCurrencySymbol",
  pure: false,
})
export class TranslateAddCurrencySymbol implements PipeTransform {
  constructor(private _store: Store<AppState>) {}

  private region: Observable<string> = this._store.pipe(
    select(getShowCurrency)
  );

  transform(value: string): any {
    let extractRegion: string;

    this.region
      .subscribe((region) => {
        extractRegion = region;
      })
      .unsubscribe();

    switch (extractRegion.toLowerCase()) {
      case "usd":
        return `\$${value}`;
      default:
        return `£${value}`;
    }
  }
}

@NgModule({
  declarations: [TranslateAddCurrencySymbol, TranslateFormatTo2DP],
  providers: [TranslateService],
  exports: [TranslateAddCurrencySymbol, TranslateFormatTo2DP],
})
export class PipeTransformModule {}
