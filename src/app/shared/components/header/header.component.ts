import { Component, OnInit } from "@angular/core";

import * as $ from "jquery";

@Component({
  selector: "showcase-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    this.runJquery();
  }

  preventDropdownNavigation($event: any) {
    $event.stopPropagation();
    $event.preventDefault();
    this.toggleCollapse($event);
  }

  // Toggle child element when clicking parent
  private toggleCollapse(event: any): void {
    $(event.target)
      .closest("li")
      .find(".dropdown-menu:first")
      .slideToggle();

    $(event.target)
      .closest("li")
      .find("span > i:first")
      .toggleClass("down");
  }

  private runJquery() {
    $("document").ready(() => {
      //close all dropdowns on click of body
      $(document).click(() => {
        $("div.dropdown-menu").slideUp();
        $("i.down").removeClass("down");
      });
    });
  }
}
