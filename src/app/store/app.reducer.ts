import { initialAppState, AppState } from "./app.state";
import { Action, AppActions } from "./type";

export function appReducer(state: AppState = initialAppState, action: Action) {
  switch (action.type) {
    case AppActions.TOGGLE_LOADING:
      return { ...state, loading: !state.loading };
    default:
      return state;
  }
}
