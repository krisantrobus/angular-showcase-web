import { initialWeatherState } from "../modules/weather-module/store/state/weather.state";
import {
  initialCoinState,
  CoinState
} from "../modules/currency-module/store/state";

export interface AppState {
  weather: any;
  loading: boolean;
  currency: CoinState;
}

export const initialAppState: AppState = {
  weather: initialWeatherState,
  loading: false,
  currency: initialCoinState
};
