import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "", redirectTo: "/weather/all", pathMatch: "full" },
  {
    path: "movies",
    loadChildren: "./modules/movie-module/movie.module#MovieModule"
  },
  {
    path: "info",
    loadChildren: "./modules/info-module/info-routing.module#InfoRoutingModule"
  },
  {
    path: "weather",
    loadChildren: "./modules/weather-module/weather.module#WeatherModule"
  },
  {
    path: "documentation",
    loadChildren:
      "./modules/documentation-module/documentation.module#DocumentationModule"
  },
  {
    path: "currency",
    loadChildren:
      "./modules/currency-module/currency-module.module#CurrencyModuleModule"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
