import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { DocumentationRoutingModule } from "./documentation-routing.module";
import { DocumentationComponent } from "./documentation/documentation.component";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { MarkdownModule } from "ngx-markdown";

@NgModule({
  declarations: [DocumentationComponent],
  imports: [
    CommonModule,
    DocumentationRoutingModule,
    HttpClientModule,
    MarkdownModule.forRoot({ loader: HttpClient })
  ]
})
export class DocumentationModule {}
