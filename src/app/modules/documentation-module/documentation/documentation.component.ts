import { Component, OnInit } from "@angular/core";

@Component({
  selector: "weather-documentation",
  template: `
    <div
      id="mdFiles"
      class="w-100 h-100 p-4"
      markdown
      [src]="'/assets/weather-documentation.md'"
    ></div>
  `,
  styleUrls: ["./documentation.component.scss"]
})
export class DocumentationComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
