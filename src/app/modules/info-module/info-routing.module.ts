import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { InfoComponent } from "./pages/movie-info/info.component";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { MarkdownModule } from "ngx-markdown";

const routes: Routes = [
  { path: "", redirectTo: "/info/movie", pathMatch: "full" },
  { path: "movie", component: InfoComponent }
];

@NgModule({
  declarations: [InfoComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    HttpClientModule,
    MarkdownModule.forRoot({ loader: HttpClient })
  ],
  exports: [RouterModule]
})
export class InfoRoutingModule {}
