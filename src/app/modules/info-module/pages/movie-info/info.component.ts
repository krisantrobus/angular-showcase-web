import { Component, OnInit } from "@angular/core";

@Component({
  selector: "movie-info",
  template: `
    <div
      id="mdFiles"
      class="w-100 h-100 p-4"
      markdown
      [src]="'/assets/movie-documentation.md'"
    ></div>
  `
})
export class InfoComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
