import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { CurrencyModuleRoutingModule } from "./currency-module-routing.module";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { coinReducer } from "./store/reducer";
import { PipeTransformModule } from "src/app/shared/utilities/translate.service";
import { HoverModule } from "src/app/shared/utilities/directives/hover.module";
import { CurrencyDataService } from "./currencies.service";
import { StoreModule } from "@ngrx/store";
import { AddCurrencyToListItemComponent } from "./components/add-currency-to-list-item/add-currency-to-list-item.component";
import { CurrencyOptionsComponent } from "./components/currency-options/currency-options.component";
import { SortByPipe } from "src/app/shared/utilities/sort-by.pipe";
import { CurrencyCardComponent } from "./components/currency-card/currency-card.component";
import { DetailCurrencyComponent } from "./pages/detail-currency/detail-currency.component";
import { AllCurrenciesComponent } from "./pages/all-currencies/all-currencies.component";
import { FilterCurrencyListPipe } from "src/app/shared/utilities/filter-currency-list.pipe";
import { CurrencyHttpInterceptor } from "./currency-http-interceptor";

@NgModule({
  declarations: [
    AllCurrenciesComponent,
    DetailCurrencyComponent,
    CurrencyCardComponent,
    SortByPipe,
    CurrencyOptionsComponent,
    AddCurrencyToListItemComponent,
    FilterCurrencyListPipe,
  ],
  imports: [
    CommonModule,
    CurrencyModuleRoutingModule,
    HttpClientModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forFeature("currency", coinReducer),
    PipeTransformModule,
    HoverModule,
  ],
  providers: [
    CurrencyDataService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CurrencyHttpInterceptor,
      multi: true,
    },
  ],
})
export class CurrencyModuleModule {}
