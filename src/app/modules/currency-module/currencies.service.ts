import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { BehaviorSubject, Subscription } from "rxjs";
import { environment } from "src/environments/environment";
import { Store, select } from "@ngrx/store";
import { CoinActions } from "./store/action.type";
import { getCoinsToSearch, getShowCurrency } from "./store/selector";
import { AppState } from "src/app/store/app.state";
import { Coin } from "src/app/interfaces/types-currency";
import { AppActions } from "src/app/store/type";
import { async } from "@angular/core/testing";

@Injectable()
export class CurrencyDataService {
  constructor(private http: HttpClient, private _store: Store<AppState>) {
    this.getSessionData();
  }

  public currencyAuthToken: BehaviorSubject<string> = new BehaviorSubject(null);
  public currencyAuthExpiry: BehaviorSubject<Date> = new BehaviorSubject(null);

  private TOKEN_KEY = "TOKEN";
  private EXPIRY_KEY = "EXPIRY";

  private coinsToSearchSubscription: Subscription;

  public getAllSpecifiedCurrencies = async () => {
    this.coinsToSearchSubscription = this._store
      .pipe(select(getCoinsToSearch))
      .subscribe(async (coinIds: string[]) => {
        coinIds.forEach(async (coinId) => {
          this.http
            .get<Coin[]>(
              `${environment.braveNewCoinApiUrl}/market-cap?assetId=${coinId}`
            )
            .subscribe(
              (response) => {
                this._store.dispatch({
                  type: CoinActions.UPDATE_COINS,
                  payload: response[0],
                });
              },
              (err) => {
                console.log(`for coin: ${coinId}`, err);
              }
            );
        });
      });
  };

  public getCoinById = (coinId: string) => {
    this._store.dispatch({
      type: AppActions.TOGGLE_LOADING,
    });

    this.http
      .get<Coin>(
        `${environment.braveNewCoinApiUrl}/market-cap?assetId=${coinId}`
      )
      .subscribe(
        (response) => {
          this._store.dispatch({
            type: CoinActions.SELECT_COIN,
            payload: response[0],
          });
          this._store.dispatch({
            type: AppActions.TOGGLE_LOADING,
          });
        },
        (err) => {
          console.log(`for coin: ${coinId}`, err);
        }
      );
  };

  public addCoinById = (coinId: string) => {
    this.http
      .get<Coin>(
        `${environment.braveNewCoinApiUrl}/market-cap?assetId=${coinId}`
      )
      .subscribe(
        (response) => {
          console.log("add coin res: ", response);

          this._store.dispatch({
            type: CoinActions.UPDATE_COINS,
            payload: response[0],
          });
        },
        (err) => {
          console.log(`for coin: ${coinId}`, err);
        }
      );
  };

  public unsubscribeCoinsToSearch = () => {
    this.coinsToSearchSubscription.unsubscribe();
  };

  public getAllCurrencySymbols = () => {
    this.http
      .get<Coin[]>(`${environment.braveNewCoinApiUrl}/asset?type=CRYPTO`)
      .subscribe(
        (response: any) => {
          this._store.dispatch({
            type: CoinActions.UPDATE_CURRENCY_LIST,
            payload: response.map((response) =>
              new Coin().setAssetDetails(response)
            ),
          });
        },
        (err) => {
          console.error(`error getting all currency symbols: `, err);
        }
      );
  };

  public authenticate = async () => {
    return await this.http
      .post(
        `${environment.braveNewCoinApiUrl}/oauth/token`,
        {
          audience: "https://api.bravenewcoin.com",
          client_id: "oCdQoZoI96ERE9HY3sQ7JmbACfBf55RY",
          grant_type: "client_credentials",
        },
        { headers: new HttpHeaders({ skip: "true" }) }
      )
      .toPromise()
      .then((tokenObj: any) => {
        const expirationTime = new Date(
          Date.now() + tokenObj.expires_in * 60 * 24
        );
        localStorage.setItem(this.TOKEN_KEY, `Bearer ${tokenObj.access_token}`);
        localStorage.setItem(this.EXPIRY_KEY, expirationTime.toString());
        this.currencyAuthToken.next(`Bearer ${tokenObj.access_token}`);
        this.currencyAuthExpiry.next(expirationTime);
        return true;
      })
      .catch((err) => {
        console.log(err);
        return false;
      });
  };

  private getSessionData() {
    this.currencyAuthToken.next(localStorage.getItem(this.TOKEN_KEY));
    this.currencyAuthExpiry.next(
      localStorage.getItem(this.EXPIRY_KEY)
        ? new Date(localStorage.getItem(this.EXPIRY_KEY))
        : null
    );
  }
}
