import { Component, OnInit, Input } from "@angular/core";
import { CurrencyDataService } from "../../currencies.service";
import { Store } from "@ngrx/store";
import { CoinActions } from "../../store/action.type";
import { AppState } from "src/app/store/app.state";
import { Coin } from "src/app/interfaces/types-currency";

@Component({
  selector: "app-add-currency-to-list-item",
  templateUrl: "./add-currency-to-list-item.component.html",
  styleUrls: ["./add-currency-to-list-item.component.scss"],
})
export class AddCurrencyToListItemComponent implements OnInit {
  @Input() public currency: Coin;

  constructor(
    private cds: CurrencyDataService,
    private _store: Store<AppState>
  ) {}

  ngOnInit() {}

  public addCoin = () => {
    this._store.dispatch({
      type: CoinActions.UPDATE_COINS_TO_SHOW,
      payload: this.currency.assetId,
    });
    this.cds.addCoinById(this.currency.assetId);
  };
}
