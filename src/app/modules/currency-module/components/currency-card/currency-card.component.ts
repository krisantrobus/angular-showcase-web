import { Component, OnInit, Input } from "@angular/core";
import { Coin } from "src/app/interfaces/types-currency";
import { Store } from "@ngrx/store";
import { CoinActions } from "../../store/action.type";
import { Router } from "@angular/router";
import { AppState } from "src/app/store/app.state";

@Component({
  selector: "app-currency-card",
  templateUrl: "./currency-card.component.html",
  styleUrls: ["./currency-card.component.scss"],
})
export class CurrencyCardComponent implements OnInit {
  @Input() public coin: Coin = null;
  constructor(private _store: Store<AppState>, private router: Router) {}

  ngOnInit() {}

  public getFormattedPrice = () => {
    return parseFloat(this.coin.price).toFixed(2);
  };

  public selectCoin = async () => {
    await this._store.dispatch({
      type: CoinActions.SELECT_COIN,
      payload: this.coin,
    });

    this.router.navigate([
      `/currency/detail/${this.coin.assetId}/${this.coin.name}/${this.coin.symbol}`,
    ]);
  };
}
