import { Component, OnInit } from "@angular/core";
import { IconDefinition } from "@fortawesome/fontawesome-svg-core";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { Store, select } from "@ngrx/store";
import { Observable } from "rxjs";
import { getShowCurrency, getCurrencyList } from "../../store/selector";
import { CoinActions } from "../../store/action.type";
import { CurrencyDataService } from "../../currencies.service";
import { AppState } from "src/app/store/app.state";

@Component({
  selector: "app-currency-options",
  templateUrl: "./currency-options.component.html",
  styleUrls: ["./currency-options.component.scss"],
})
export class CurrencyOptionsComponent implements OnInit {
  constructor(
    private _store: Store<AppState>,
    private cds: CurrencyDataService
  ) {}

  public faSearch: IconDefinition = faSearch;
  public placeholder: string = "Add currency using symbol or name";

  public isSearching: boolean;
  public searchValue: string;

  public currencyList: Observable<Object[]> = this._store.pipe(
    select(getCurrencyList)
  );
  public currencytoSearch: Observable<string> = this._store.pipe(
    select(getShowCurrency)
  );

  ngOnInit() {
    this.getCurrencySymbols();
  }

  public getCurrencySymbols = () => {
    this.currencyList.forEach((list) => {
      if (list.length === 0) {
        this.cds.getAllCurrencySymbols();
      }
    });
  };

  public updateShowCurrency = (currency: string) => {
    this._store.dispatch({
      type: CoinActions.UPDATE_SHOW_CURRENCY,
      payload: currency,
    });
    this.cds.getAllSpecifiedCurrencies();
  };

  public isShowCurrency = (currencyValue: string) => {
    let isCurrency: boolean;
    this.currencytoSearch.forEach((currency) => {
      isCurrency = currency === currencyValue;
    });

    return isCurrency;
  };

  public search = (searchString: string) => {
    this.isSearching = searchString.length > 0 ? true : false;
  };

  public hideList = () => {
    this.isSearching = false;
  };
}
