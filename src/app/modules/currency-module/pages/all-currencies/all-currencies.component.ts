import { Component, OnInit, OnDestroy } from "@angular/core";
import { CurrencyDataService } from "../../currencies.service";
import { Observable } from "rxjs";
import { Store, select } from "@ngrx/store";
import { Coin } from "src/app/interfaces/types-currency";
import { getCurrencyList, getStoreCoins } from "../../store/selector";
import { AppState } from "src/app/store/app.state";

@Component({
  selector: "app-all-currencies",
  templateUrl: "./all-currencies.component.html",
  styleUrls: ["./all-currencies.component.scss"],
})
export class AllCurrenciesComponent implements OnInit, OnDestroy {
  constructor(
    private cds: CurrencyDataService,
    private _store: Store<AppState>
  ) {}

  public coins: Observable<Coin[]>;
  public coinList: Observable<Coin[]>;

  ngOnInit() {
    // Uses async pipe to unsubscribe
    this.coins = this._store.pipe(select(getStoreCoins));
    this.coinList = this._store.pipe(select(getCurrencyList));

    this.getAllCoins();
  }

  ngOnDestroy() {
    this.cds.unsubscribeCoinsToSearch();
  }

  private getAllCoins = async () => {
    this.coinList.subscribe((val) => {
      if (val.length > 0) {
        this.coins.forEach((coins: Coin[]) => {
          if (coins.length === 0) {
            this.cds.getAllSpecifiedCurrencies();
          }
        });
      }
    });
  };
}
