// import { async, ComponentFixture, TestBed } from "@angular/core/testing";
// import { RouterTestingModule } from "@angular/router/testing";
// import { DetailCurrencyComponent } from "./detail-currency.component";
// import { HttpClientTestingModule } from "@angular/common/http/testing";
// import {
//   PipeTransformModule,
//   TranslateService,
// } from "src/app/utilities/translate.service";
// import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
// import { CommonModule } from "@angular/common";
// import { provideMockStore, MockStore } from "@ngrx/store/testing";
// import { CurrencyDataService } from "../../currencies.service";
// import { AppStateRoot } from "src/app/store/state";
// import { Store } from "@ngrx/store";
// import { Coin } from "src/app/interfaces/types-currency";
// import { getSelectedCoin, getShowCurrency } from "../../store/selector";
// import { ActivatedRoute } from "@angular/router";
// import { By } from "@angular/platform-browser";

// describe("DetailCurrencyComponent", () => {
//   let component: DetailCurrencyComponent;
//   let fixture: ComponentFixture<DetailCurrencyComponent>;

//   let store: MockStore<AppStateRoot>;
//   let currencyService: CurrencyDataService;

//   let mockRouterParams: Object = {
//     snapshot: {
//       params: { coinId: "BTC" },
//     },
//   };

//   let mockCoin: Coin = {
//     symbol: "BTC",
//     name: "Mock Currency",
//     price: "988523.01345544",
//     volume: "4000",
//     name: null,
//     price: null,
//     source: null,
//     success: null,
//     time_stamp: new Date().toString(),
//     utc_date: null,
//     vol_24hr_pcnt: null,
//   };

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [DetailCurrencyComponent],
//       imports: [
//         PipeTransformModule,
//         FontAwesomeModule,
//         CommonModule,
//         RouterTestingModule,
//         HttpClientTestingModule,
//       ],
//       providers: [
//         TranslateService,
//         provideMockStore(),
//         CurrencyDataService,
//         {
//           provide: ActivatedRoute,
//           useValue: mockRouterParams,
//         },
//       ],
//     }).compileComponents();

//     store = TestBed.get(Store);
//     currencyService = TestBed.get(CurrencyDataService);
//     store.overrideSelector(getShowCurrency, "usd");
//     store.overrideSelector(getSelectedCoin, mockCoin);
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(DetailCurrencyComponent);
//     component = fixture.componentInstance;
//     currencyService.getCoinById = jasmine.createSpy("getCoinById");
//     component.calculateQuantity = jasmine.createSpy("calculateQuantity");
//     fixture.detectChanges();
//   });

//   it("should create", () => {
//     expect(component).toBeTruthy();
//   });

//   describe("with selectedCoin in store", () => {
//     it("should not call api if selectedCoin in store", () => {
//       expect(currencyService.getCoinById).not.toHaveBeenCalled();
//     });

//     describe("should call api", () => {
//       beforeEach(() => {
//         fixture.destroy();
//         fixture = TestBed.createComponent(DetailCurrencyComponent);
//         component = fixture.componentInstance;
//         store.overrideSelector(getSelectedCoin, null);
//         fixture.detectChanges();
//       });
//       it("if selectedCoin not in store", () => {
//         expect(currencyService.getCoinById).toHaveBeenCalledWith("BTC");
//       });
//     });
//     describe("should call api if selectedCoin in store", () => {
//       beforeEach(() => {
//         TestBed.get(ActivatedRoute).snapshot.params = { coinId: "MCK" };
//         fixture.destroy();
//         fixture = TestBed.createComponent(DetailCurrencyComponent);
//         component = fixture.componentInstance;
//         fixture.detectChanges();
//       });
//       it("with different id to router", () => {
//         expect(currencyService.getCoinById).toHaveBeenCalledWith("MCK");
//       });
//     });
//   });

//   describe("Check html renders correctly with correct", () => {
//     it("title", () => {
//       const header = fixture.debugElement.query(By.css("div.card-header"));
//       expect(header.nativeElement.textContent.trim()).toContain(
//         mockCoin.coin_name
//       );
//     });
//     it("last value", () => {
//       const header = fixture.debugElement.queryAll(By.css("p"));
//       expect(header[0].nativeElement.textContent.trim()).toContain("988523.01");
//     });
//   });

//   describe("Should call calculateQuantity", () => {
//     it("with 100", () => {
//       expect(component.calculateQuantity).toHaveBeenCalledWith(
//         mockCoin.last_price,
//         100
//       );
//     });
//     it("with 250", () => {
//       expect(component.calculateQuantity).toHaveBeenCalledWith(
//         mockCoin.last_price,
//         250
//       );
//     });
//     it("with 5000", () => {
//       expect(component.calculateQuantity).toHaveBeenCalledWith(
//         mockCoin.last_price,
//         5000
//       );
//     });
//   });
// });
