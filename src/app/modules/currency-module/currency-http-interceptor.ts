import { Injectable } from "@angular/core";
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
} from "@angular/common/http";

import { from, Observable, throwError } from "rxjs";
import { map, switchMap } from "rxjs/operators";
import { CurrencyDataService } from "./currencies.service";
import { environment } from "src/environments/environment";

@Injectable()
export class CurrencyHttpInterceptor implements HttpInterceptor {
  private authToken: string;
  private authExpiry: Date;

  constructor(private cds: CurrencyDataService) {
    this.cds.currencyAuthToken.subscribe((token) => (this.authToken = token));
    this.cds.currencyAuthExpiry.subscribe(
      (expiry) => (this.authExpiry = expiry)
    );
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // Use skip header to bypass authentication check
    if (req.headers.get("skip")) {
      return next.handle(this.reqWithHeaders(req));
    }

    return from(this.handleAuthentication(req)).pipe(
      switchMap((success) => {
        if (success) {
          return next.handle(this.reqWithHeaders(req)).pipe(
            map((event) => {
              if (event instanceof HttpResponse && event.body.content) {
                return event.clone({ body: event.body.content });
              }
              return event;
            })
          );
        } else {
          return throwError("Unauthenticated");
        }
      })
    );
  }

  /** reqWithHeaders
   * Accepts a request which will add appropriate headers to
   *
   * @param req: the origional request
   */
  private reqWithHeaders = (req: HttpRequest<any>) => {
    return req.clone({
      setHeaders: {
        Accept: "application/json",
        "x-rapidapi-host": environment.braveNewCoinApiHostHeader,
        "x-rapidapi-key": environment.braveNewCoinApiKeyHeader,
        authorization: this.authToken ? this.authToken : "",
      },
    });
  };

  /** handleAuthentication
   * Checks the authentication validity and authenticates if token is expired or not present
   */
  private handleAuthentication = async (req: HttpRequest<any>) => {
    if (
      this.authToken &&
      this.authExpiry &&
      new Date(Date.now()) < this.authExpiry
    ) {
      return true;
    } else {
      const success = await this.cds.authenticate();
      return success;
    }
  };
}
