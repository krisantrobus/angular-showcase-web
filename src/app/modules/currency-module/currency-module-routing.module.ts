import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DetailCurrencyComponent } from "./pages/detail-currency/detail-currency.component";
import { AllCurrenciesComponent } from "./pages/all-currencies/all-currencies.component";

const routes: Routes = [
  {
    path: "",
    component: AllCurrenciesComponent,
  },
  {
    path: "detail/:coinId/:name/:symbol",
    component: DetailCurrencyComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CurrencyModuleRoutingModule {}
