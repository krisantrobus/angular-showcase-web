import { Coin } from "src/app/interfaces/types-currency";
import { COINS } from "src/app/interfaces/const";

export interface CoinState {
  coins: Coin[];
  selectedCoin: Coin;
  coinsToSearch: string[];
  showCurrency: string;
  listOfCurrencies: Coin[];
}

export const initialCoinState: CoinState = {
  coins: [],
  selectedCoin: null,
  coinsToSearch: [
    COINS.tether,
    COINS.bitcoin,
    COINS.bitcoinCash,
    COINS.litecoin,
  ],
  showCurrency: "usd",
  listOfCurrencies: [],
};
