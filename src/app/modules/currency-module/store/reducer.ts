import { CoinActions } from "./action.type";
import { Coin } from "src/app/interfaces/types-currency";
import { CoinState, initialCoinState } from "./state";
import { Action } from "src/app/store/type";

export function coinReducer(
  state: CoinState = initialCoinState,
  action: Action
) {
  switch (action.type) {
    case CoinActions.SELECT_COIN:
      return { ...state, selectedCoin: action.payload };
    case CoinActions.UPDATE_COINS:
      return {
        ...state,
        coins: updateCoin(action.payload, state.coins, state.listOfCurrencies),
      };
    case CoinActions.UPDATE_SHOW_CURRENCY:
      return { ...state, showCurrency: action.payload };
    case CoinActions.UPDATE_CURRENCY_LIST:
      return { ...state, listOfCurrencies: action.payload };
    case CoinActions.UPDATE_COINS_TO_SHOW:
      return {
        ...state,
        coinsToSearch: updateCoinsToShow(action.payload, state.coinsToSearch),
      };
    default: {
      return state;
    }
  }
}

const updateCoin = (coin: Coin, coins: Coin[], lookupList: Coin[]) => {
  const coinExists: boolean =
    coins.filter((coinItem) => coin.assetId === coinItem.assetId).length > 0
      ? true
      : false;

  lookupList.map((coinList) => {
    if (coin.assetId === coinList.assetId) {
      coinList.setTickerDetails(coin);
      if (coinExists) {
        coins = coins.map((coinItem) => {
          if (coinList.assetId === coinItem.assetId) {
            return coinList;
          }
        });
      } else {
        coins.push(coinList);
      }
    }
  })[0];

  return coins;
};

const updateCoinsToShow = (coinId: string, coinIds: string[]) => {
  const coinExists: boolean =
    coinIds.filter((coinIdListItem) => coinId == coinIdListItem).length > 0
      ? true
      : false;

  if (coinExists) {
    return coinIds;
  } else {
    coinIds.push(coinId);
  }
  return coinIds;
};
