import { createSelector } from "@ngrx/store";
import { CoinState } from "./state";
import { AppState } from "src/app/store/app.state";

const selectCoinFeature = (state: AppState) => state.currency;

export const getStoreCoins = createSelector(
  selectCoinFeature,
  (state: CoinState) => state.coins
);

export const getSelectedCoin = createSelector(
  selectCoinFeature,
  (state: CoinState) => state.selectedCoin
);

export const getCoinsToSearch = createSelector(
  selectCoinFeature,
  (state: CoinState) => state.coinsToSearch
);

export const getShowCurrency = createSelector(
  selectCoinFeature,
  (state: CoinState) => state.showCurrency
);

export const getCurrencyList = createSelector(
  selectCoinFeature,
  (state: CoinState) => state.listOfCurrencies
);
