import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MovieHomeComponent } from "./pages/movie-home/movie-home.component";

const routes: Routes = [
  { path: "", redirectTo: "/movies/recent", pathMatch: "full" },
  { path: "recent", component: MovieHomeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MovieRoutingModule {}
