import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalMetaComponent } from './modal-meta.component';

@NgModule({
  declarations: [ModalMetaComponent],
  exports: [ModalMetaComponent],
  imports: [
    CommonModule
  ]
})
export class ModalMetaModule { }
