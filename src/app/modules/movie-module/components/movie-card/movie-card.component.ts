import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Movie } from "../../model";

@Component({
  selector: "movie-movie-card",
  templateUrl: "./movie-card.component.html",
  styleUrls: ["./movie-card.component.scss"]
})
export class MovieCardComponent implements OnInit {
  @Input() public movie: Movie;
  @Output("selectMovie") private selectMovieEmit: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  ngOnInit() { }

  // emit to select a movie. There is an output data as it's being passed in from the parent with no data change
  selectMovie() {
    this.selectMovieEmit.emit();
  }
}
