import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalComponent } from './modal.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DUMMY_MOVIE } from '../../../../test-models/dummy';
import { By } from '@angular/platform-browser';

describe('ModalComponent', () => {
  let component: ModalComponent;
  let fixture: ComponentFixture<ModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalComponent],
      imports: [BrowserAnimationsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should toggle active variable', () => {
    component.active = true;
    fixture.detectChanges();
    component.toggleShow();
    expect(component.active).toEqual(false);
  });

  it('should hide modal', () => {
    component.movie = DUMMY_MOVIE;
    fixture.detectChanges();
    const modalCompiled = fixture.debugElement.nativeElement;
    expect(modalCompiled.querySelector("#modal-container").style.visibility).toBe('hidden');

  });

  it('should show modal', () => {
    component.movie = DUMMY_MOVIE;
    component.active = true;
    fixture.detectChanges();
    const modalCompiled = fixture.debugElement.nativeElement;
    expect(modalCompiled.querySelector("#modal-container").hidden).toBe(false);
  });

  it('should render movie', () => {
    component.movie = DUMMY_MOVIE;
    component.active = true;
    fixture.detectChanges();
    const modalCompiled = fixture.debugElement.nativeElement;
    expect(modalCompiled.querySelector('h4').textContent).toContain(DUMMY_MOVIE.title);
  });
});
