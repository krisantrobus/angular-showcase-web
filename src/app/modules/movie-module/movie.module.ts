import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MoviesDataService } from "./movies-data.service";
import { HttpClientModule } from "@angular/common/http";
import { MovieHomeComponent } from "./pages/movie-home/movie-home.component";
import { MovieCardComponent } from "./components/movie-card/movie-card.component";
import { CommonModule } from "@angular/common";
import { CardDirectiveModule } from "src/app/shared/directives/card-hover-shadow/card-directive.module";
import { ModalComponent } from "./components/modal/modal.component";
import { LoadingModule } from "../../shared/components/loading/loading.module";
import { MovieRoutingModule } from "./movie-routing.module";
import { ModalMetaModule } from "./components/modal-meta/modal-meta.module";
import { HeaderComponent } from "./components/header/header.component";

@NgModule({
  declarations: [
    MovieHomeComponent,
    MovieCardComponent,
    ModalComponent,
    HeaderComponent
  ],
  providers: [MoviesDataService, HttpClientModule],
  imports: [
    MovieRoutingModule,
    HttpClientModule,
    CommonModule,
    CardDirectiveModule,
    LoadingModule,
    ModalMetaModule
  ],
  exports: [RouterModule]
})
export class MovieModule {}
