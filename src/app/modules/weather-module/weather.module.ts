import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { WeatherHomeComponent } from "./pages/weather-home/weather-home.component";
import { HttpClientModule } from "@angular/common/http";
import { WeatherDataService } from "./weather-data.service";
import { WeatherCardComponent } from "./components/weather-card/weather-card.component";
import { StoreModule } from "@ngrx/store";
import { weatherReducer } from "./store/reducers/weather.reducer";
import { CityModalComponent } from "./components/city-modal/city-modal.component";
import { WeatherRoutingModule } from "./weather-routing.module";
import { HourlyForecastCardComponent } from "./components/hourly-forecast-card/hourly-forecast-card.component";
import { CardHoverModule } from "src/app/shared/directives/card-hover-up/card-hover.module";
import { MapHomeComponent } from "./pages/map-home/map-home.component";
import { LeafletModule } from "@asymmetrik/ngx-leaflet";

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    CardHoverModule,
    WeatherRoutingModule,
    StoreModule.forFeature("weather", weatherReducer),
    LeafletModule
  ],
  providers: [WeatherDataService],
  declarations: [
    WeatherHomeComponent,
    WeatherCardComponent,
    CityModalComponent,
    HourlyForecastCardComponent,
    HourlyForecastCardComponent,
    MapHomeComponent
  ]
})
export class WeatherModule {}
