import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { WeatherCardComponent } from "./weather-card.component";
import { MOCK_WEATHER } from "../../../../test/mock";

describe("WeatherCardComponent", () => {
  let component: WeatherCardComponent;
  let fixture: ComponentFixture<WeatherCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WeatherCardComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should render weather", () => {
    component.city = MOCK_WEATHER;
    fixture.detectChanges();
    const weatherCard = fixture.debugElement.nativeElement;
    expect(weatherCard.querySelector("div").textContent).toContain(
      MOCK_WEATHER.name
    );
  });
});
