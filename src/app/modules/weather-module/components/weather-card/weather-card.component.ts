import { Component, OnInit, Input, Output } from "@angular/core";
import { Weather } from "../../model";
import { EventEmitter } from "@angular/core";

@Component({
  selector: "weather-card",
  templateUrl: "./weather-card.component.html",
  styleUrls: ["./weather-card.component.scss"]
})
export class WeatherCardComponent implements OnInit {
  @Input() public city: Weather;
  @Output("toggleModal") private emitToggle: EventEmitter<
    any
  > = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {}

  toggleModal() {
    this.emitToggle.emit();
  }
}
