import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { WeatherDataService } from "../../weather-data.service";
import { Store } from "@ngrx/store";
import { Subscription } from "rxjs";
import { WeatherActions } from "../../store/types";
import { Weather } from "../../model";
import { CityModalComponent } from "../../components/city-modal/city-modal.component";
import { AppState } from "../../../../store/app.state";
import { getCitiesList } from "../../store/selectors/weather.selector";

@Component({
  selector: "weather-home",
  templateUrl: "./weather-home.component.html",
  styleUrls: ["./weather-home.component.scss"]
})
export class WeatherHomeComponent implements OnInit, OnDestroy {
  @ViewChild("cityModal") private cityModal: CityModalComponent;
  constructor(
    private wdataServ: WeatherDataService,
    private _store: Store<AppState>
  ) {}

  private sub: Subscription = null;
  private subPopulateStore: Subscription = null;
  citiesData: Weather[] = null;
  selectedCity: Weather = null;

  ngOnInit() {
    this.sub = this._store.select(getCitiesList).subscribe(cities => {
      this.citiesData = cities;
    });

    this.subPopulateStore = this.wdataServ.getCurrentGroupWeather().subscribe(
      data => {
        this._store.dispatch({
          type: WeatherActions.UPDATE_CITIES,
          payload: <Weather[]>data.list
        });
      },
      err => console.log(`error contating api: `, err)
    );
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
    this.subPopulateStore.unsubscribe();
  }

  // show the model, set teh selected city int the store and retrieve and set the state forecast
  toggleModal(city: Weather) {
    this._store.dispatch({
      type: WeatherActions.SELECT_CITY,
      payload: city
    });

    this.wdataServ.getHourlyDataForCityId(city.id).subscribe(
      forecast => {
        this._store.dispatch({
          type: WeatherActions.UPDATE_FORECAST,
          payload: forecast
        });
      },
      err => console.log(`error contating api: `, err)
    );
    this.cityModal.toggleShow();
  }
}
