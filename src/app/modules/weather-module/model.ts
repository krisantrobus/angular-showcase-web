interface WeatherNest {
  id: number;
  description: string;
  icon: string;
  main: string;
}

interface WeatherMain {
  humidity: number;
  pressure: number;
  temp: number;
  temp_max: number;
  temp_min: number;
}

interface WeatherSys {
  country: string;
  sunrise: number;
  sunset: number;
  timezone: number;
}

interface WeatherWind {
  deg: number;
  speed: number;
}

export interface Coords {
  lon: number;
  lat: number;
}

export interface Weather {
  clouds: {
    all: number;
  };
  coord?: Coords;
  dt?: number;
  id: number;
  main: WeatherMain;
  name: string;
  sys?: WeatherSys;
  visibility: number;
  weather: WeatherNest[];
  wind: WeatherWind;
}

interface CityForecast {
  id: number;
  name: string;
  coord?: Coords;
  country: string;
  timezone: number;
}

interface ForecastMain {
  grnd_level: number;
  humidity: number;
  pressure: number;
  sea_level: number;
  temp: number;
  temp_kf: number;
  temp_max: number;
  temp_min: number;
}

export interface ForecastObj {
  clouds: {
    all: number;
  };
  dt: number;
  dt_txt: string;
  main: ForecastMain[];
  rain?: {
    //couldn't define a label 3h as it start with a number
    [key: string]: number;
  };
  sys: {
    pod: string;
  };
  weather: WeatherNest[];
  wind: WeatherWind;
}

export interface WeatherForecast {
  city: CityForecast;
  cnt: number;
  cod: string;
  message: number;
  list: ForecastObj[];
}
