import { Weather, WeatherForecast } from "../../model";

export interface WeatherState {
  cities: Weather[];
  selectedCity: Weather;
  selectedCityForecast: WeatherForecast;
}

export const initialWeatherState: WeatherState = {
  cities: null,
  selectedCity: null,
  selectedCityForecast: null
};
