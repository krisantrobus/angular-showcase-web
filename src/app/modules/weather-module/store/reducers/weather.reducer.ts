import { WeatherActions } from "../types";
import { initialAppState } from "../../../../store/app.state";
import { Action } from "../../../../store/type";
import { ForecastObj } from "../../model";

export function weatherReducer(
  state: Object = initialAppState,
  action: Action
) {
  switch (action.type) {
    case WeatherActions.SELECT_CITY:
      return { ...state, selectedCity: action.payload };
    case WeatherActions.UPDATE_FORECAST:
      return { ...state, selectedCityForecast: action.payload };
    case WeatherActions.UPDATE_CITIES:
      return { ...state, cities: action.payload };
    default:
      return state;
  }
}
