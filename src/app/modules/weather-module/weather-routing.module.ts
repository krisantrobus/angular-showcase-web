import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { WeatherHomeComponent } from "./pages/weather-home/weather-home.component";
import { WeatherModule } from "./weather.module";
import { MapHomeComponent } from "./pages/map-home/map-home.component";

const routes: Routes = [
  { path: "", redirectTo: "/weather/all", pathMatch: "full" },
  { path: "all", component: WeatherHomeComponent },
  { path: "map", component: MapHomeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WeatherRoutingModule {}
